package main

import (
	"log"
	"net/http"
)

var version = "-"

func main() {
	m := http.NewServeMux()
	m.Handle("/version", http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte(`{"version": "` + version + `"}`))
	}))
	if err := http.ListenAndServe(":8080", m); err != nil {
		log.Print(err)
	}
}
